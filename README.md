Existe um erro ao compilar, acredito que seja devido a minha tentativa de criar a tabela mestre detalhe em
/postgres-demo/src/main/java/com/example/postgresdemo/model/DetalhePedido.java

O erro é:
[ERROR] Errors: 
[ERROR]   PostgresDemoApplicationTests.contextLoads » IllegalState Failed to load Applic...

**Os endpoint de cada operação são:**

***Cadastro de clientes:***
GET /clientes
POST /clientes
PUT /clientes/{clientId}
DELETE /clientes/{clientId}

***Campos tabela Clientes:***
id_cliente Longint
nome_cliente String(150)
situacao_cliente String(1)
tipo_cliente String(1)

***Cadastro de produtos***
GET /produtos
POST /produtos
PUT /produtos/{produtosId}
DELETE /produtos/{produtosId}

***Campos da tabela de Produtos:***
id_produto Longint
codigo_produto Longint
descricao_produto String(150)
situacao_produto String(1)
tipo_produto String(1)
valor_produto Float

***Cadastro de pedidos:***
GET /pedidos
POST /pedidos
PUT /pedidos/{pedidoId}
DELETE /pedidos/{pedidoId}

***Campos da tabela Mestre de Pedidos:***
id_pedido Longint
valor_produtos Float
valor_desconto Float
valor_acrescimo Float
valor_total Float
situacao_pedido String(1)

***Cadastro de itens de pedido:***
GET /pedidos/{pedidoId}/item
POST /pedidos/{pedidoId}/item
PUT /pedidos/{pedidoId}/item/{itemId}
DELETE /pedidos/{pedidoId}/item/{itemId}

***Campos da tabela Detalhe de Pedidos:***
id_pedido Longint
id_cliente Longint
id_produto Longint
valor_produtos Float
valor_desconto Float
valor_acrescimo Float
valor_sub_total Float
situacao_produto String(1)
