package com.example.postgresdemo.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Produtos")
public class Produtos extends AuditModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1382372353240329196L;

	@Id
    @GeneratedValue(generator = "gen_id_produto")
	@Column(name="id_produto")
    @SequenceGenerator(
            name = "gen_id_produto",
            sequenceName = "id_produto_sequence",
            initialValue = 1000
    )
    private Long id;
	
    @NotBlank
    @Size(min = 3, max = 150)
    private Long codigo_produto;
    
    @NotBlank
    @Size(min = 3, max = 150)
    private Long descricao_produto;

    @NotBlank
    @Size(min = 1, max = 1)
    private String situacao_produto;
    
    @NotBlank
    @Size(min = 1, max = 1)
    private String tipo_produto;
    
    @NotBlank
    private Float valor_produto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCodigo_produto() {
		return codigo_produto;
	}

	public void setCodigo_produto(Long codigo_produto) {
		this.codigo_produto = codigo_produto;
	}

	public Long getDescricao_produto() {
		return descricao_produto;
	}

	public void setDescricao_produto(Long descricao_produto) {
		this.descricao_produto = descricao_produto;
	}

	public String getSituacao_produto() {
		return situacao_produto;
	}

	public void setSituacao_produto(String situacao_produto) {
		this.situacao_produto = situacao_produto;
	}

	public String getTipo_produto() {
		return tipo_produto;
	}

	public void setTipo_produto(String tipo_produto) {
		this.tipo_produto = tipo_produto;
	}

	public Float getValor_produto() {
		return valor_produto;
	}

	public void setValor_produto(Float valor_produto) {
		this.valor_produto = valor_produto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    

}