package com.example.postgresdemo.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Clientes")
public class Clientes extends AuditModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -981300641921450562L;
	
	@Id
    @GeneratedValue(generator = "gen_id_cliente")
	@Column(name="id_cliente")
    @SequenceGenerator(
            name = "gen_id_cliente",
            sequenceName = "id_cliente_sequence",
            initialValue = 1000
    )
    private Long id;
	
    @NotBlank
    @Size(min = 3, max = 150)
    private String nome_cliente;

    @NotBlank
    @Size(min = 1, max = 1)
    private String situacao_cliente;
    
    @NotBlank
    @Size(min = 1, max = 1)
    private String tipo_cliente;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome_cliente() {
		return nome_cliente;
	}

	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}

	public String getSituacao_cliente() {
		return situacao_cliente;
	}

	public void setSituacao_cliente(String situacao_cliente) {
		this.situacao_cliente = situacao_cliente;
	}

	public String getTipo_cliente() {
		return tipo_cliente;
	}

	public void setTipo_cliente(String tipo_cliente) {
		this.tipo_cliente = tipo_cliente;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
	
    
}
