package com.example.postgresdemo.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "DetalhePedido")
public class DetalhePedido extends AuditModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3411677679387574661L;

	@Id
    @GeneratedValue(generator = "gen_id_detped")
	@Column(name="id_item")
    @SequenceGenerator(
            name = "gen_id_detped",
            sequenceName = "id_detped_sequence",
            initialValue = 1000
    )
	private Long id;
	
	/*Faz a ligação com a tabela dos produtos e cliente */
	/*@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="id_cliente", referencedColumnName="id_cliente")
    @OnDelete(action = OnDeleteAction.CASCADE)*/
    @JsonIgnore
    private Clientes cliente;
	
    /*Faz a ligação com a tabela dos produtos e cliente */
    /*@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="id_produto", referencedColumnName="id_produto")*/
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Produtos produto;
	
    /*Faz a ligação com a tabela dos produtos e cliente */
    /*@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="id_pedido", referencedColumnName="id_pedido")*/
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private MestrePedido pedido;
	
    @NotBlank
    private Float valor_produtos;
    
    @NotBlank
    private Float valor_desconto;
    
    @NotBlank
    private Float valor_acrescimo;
    
    @NotBlank
    private Float valor_sub_total;
    
    @NotBlank
    @Size(min = 1, max = 1)
    private String situacao_produto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public Produtos getProduto() {
		return produto;
	}

	public void setProduto(Produtos produto) {
		this.produto = produto;
	}

	public MestrePedido getPedido() {
		return pedido;
	}

	public void setPedido(MestrePedido pedido) {
		this.pedido = pedido;
	}

	public Float getValor_produtos() {
		return valor_produtos;
	}

	public void setValor_produtos(Float valor_produtos) {
		this.valor_produtos = valor_produtos;
	}

	public Float getValor_desconto() {
		return valor_desconto;
	}

	public void setValor_desconto(Float valor_desconto) {
		this.valor_desconto = valor_desconto;
	}

	public Float getValor_acrescimo() {
		return valor_acrescimo;
	}

	public void setValor_acrescimo(Float valor_acrescimo) {
		this.valor_acrescimo = valor_acrescimo;
	}

	public Float getValor_sub_total() {
		return valor_sub_total;
	}

	public void setValor_sub_total(Float valor_sub_total) {
		this.valor_sub_total = valor_sub_total;
	}

	public String getSituacao_produto() {
		return situacao_produto;
	}

	public void setSituacao_produto(String situacao_produto) {
		this.situacao_produto = situacao_produto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    
}