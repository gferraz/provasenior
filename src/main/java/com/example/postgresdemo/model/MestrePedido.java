package com.example.postgresdemo.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "MestrePedido")
public class MestrePedido extends AuditModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4779595484026535697L;

	@Id
    @GeneratedValue(generator = "gen_id_pedido")
	@Column(name="id_pedido")
    @SequenceGenerator(
            name = "gen_id_pedido",
            sequenceName = "id_pedido_sequence",
            initialValue = 1000
    )
	private Long id;
	
    
    @NotBlank
    private Float valor_produtos;
    
    @NotBlank
    private Float valor_desconto;
    
    @NotBlank
    private Float valor_acrescimo;
    
    @NotBlank
    private Float valor_total;
    
    @NotBlank
    @Size(min = 1, max = 1)
    private String situacao_pedido;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getValor_produtos() {
		return valor_produtos;
	}

	public void setValor_produtos(Float valor_produtos) {
		this.valor_produtos = valor_produtos;
	}

	public Float getValor_desconto() {
		return valor_desconto;
	}

	public void setValor_desconto(Float valor_desconto) {
		this.valor_desconto = valor_desconto;
	}

	public Float getValor_acrescimo() {
		return valor_acrescimo;
	}

	public void setValor_acrescimo(Float valor_acrescimo) {
		this.valor_acrescimo = valor_acrescimo;
	}

	public Float getValor_total() {
		return valor_total;
	}

	public void setValor_total(Float valor_total) {
		this.valor_total = valor_total;
	}

	public String getSituacao_pedido() {
		return situacao_pedido;
	}

	public void setSituacao_pedido(String situacao_pedido) {
		this.situacao_pedido = situacao_pedido;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    
	
	
}