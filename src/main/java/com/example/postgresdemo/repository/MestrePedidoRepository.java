package com.example.postgresdemo.repository;

import com.example.postgresdemo.model.MestrePedido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MestrePedidoRepository extends JpaRepository<MestrePedido, Long> {
}
