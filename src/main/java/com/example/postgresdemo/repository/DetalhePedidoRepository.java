package com.example.postgresdemo.repository;

import com.example.postgresdemo.model.DetalhePedido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DetalhePedidoRepository extends JpaRepository<DetalhePedido, Long> {
    List<DetalhePedido> findByPedidoId(Long pedidoId);
}
