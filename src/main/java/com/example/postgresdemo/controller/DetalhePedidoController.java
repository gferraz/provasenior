package com.example.postgresdemo.controller;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model.DetalhePedido;
import com.example.postgresdemo.model.MestrePedido;
import com.example.postgresdemo.model.Produtos;
import com.example.postgresdemo.repository.DetalhePedidoRepository;
import com.example.postgresdemo.repository.MestrePedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class DetalhePedidoController {

	@Autowired
	private DetalhePedidoRepository detalhePedidoRepository;

	@Autowired
	private MestrePedidoRepository mestrePedidoRepository;

	@GetMapping("/pedidos/{pedidoId}/item")
	public List<DetalhePedido> getItemByPedidoId(@PathVariable Long pedidoId) {
		return detalhePedidoRepository.findByPedidoId(pedidoId);
	}

	@PostMapping("/pedidos/{pedidoId}/item")
	public DetalhePedido addItem(@PathVariable Long pedidoId, @Valid @RequestBody DetalhePedido item) {
		return mestrePedidoRepository.findById(pedidoId).map(pedido -> {
			item.setPedido(pedido);
			return detalhePedidoRepository.save(item);
		}).orElseThrow(() -> new ResourceNotFoundException("Pedido not found with id " + pedidoId));
	}

	@PutMapping("/pedidos/{pedidoId}/item/{itemId}")
	public DetalhePedido updateItem(@PathVariable Long pedidoId, @PathVariable Long itemId,
			@Valid @RequestBody DetalhePedido detalhePedidoRequest) {
		if (!mestrePedidoRepository.existsById(pedidoId)) {
			throw new ResourceNotFoundException("Pedido not found with id " + pedidoId);
		}

		return detalhePedidoRepository.findById(itemId).map(item -> {
			
			item.setValor_acrescimo(detalhePedidoRequest.getValor_acrescimo());
			/*item.setValor_desconto(detalhePedidoRequest.getValor_desconto());*/
			item.setValor_produtos(detalhePedidoRequest.getValor_produtos());
			item.setValor_sub_total(detalhePedidoRequest.getValor_sub_total());
			item.setProduto(detalhePedidoRequest.getProduto());
			item.setCliente(detalhePedidoRequest.getCliente());
			item.setPedido(detalhePedidoRequest.getPedido());
			Produtos new_produto = detalhePedidoRequest.getProduto();
			Optional<MestrePedido> pedido_existente = mestrePedidoRepository.findById(pedidoId);
			if(new_produto.getSituacao_produto()!="A") {
				new ResourceNotFoundException("O produto não pode ser adicionado ao pedido pois está INATIVADO.");
			}
			if (detalhePedidoRequest.getValor_desconto() > 0) {
				if(pedido_existente.get().getSituacao_pedido()!="A") {
					new ResourceNotFoundException("Desconto não aplicado, o pedido não está com a situação Aberto.");
				}
				if (new_produto.getTipo_produto() != "P") {
					new ResourceNotFoundException("O item não é do tipo Produto, desconto não aceito: itemId =" + itemId);
				}
				item.setValor_desconto(detalhePedidoRequest.getValor_desconto());
				return detalhePedidoRepository.save(item);
			} else {
				return detalhePedidoRepository.save(item);
			}
		}).orElseThrow(() -> new ResourceNotFoundException("Item do pedido not found with id " + itemId));
	}

	@DeleteMapping("/pedidos/{pedidoId}/item/{itemId}")
	public ResponseEntity<?> deleteAnswer(@PathVariable Long pedidoId, @PathVariable Long itemId) {
		if (!mestrePedidoRepository.existsById(pedidoId)) {
			throw new ResourceNotFoundException("Pedido not found with id " + pedidoId);
		}

		return detalhePedidoRepository.findById(itemId).map(item -> {
			detalhePedidoRepository.delete(item);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("Item de pedido not found with id " + itemId));

	}
}
