package com.example.postgresdemo.controller;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model.Clientes;
import com.example.postgresdemo.repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
public class ClientesController {

    @Autowired
    private ClientesRepository clientesRepository;

    @GetMapping("/clientes")
    public Page<Clientes> getCliente(Pageable pageable) {
        return clientesRepository.findAll(pageable);
    }


    @PostMapping("/clientes")
    public Clientes createCliente(@Valid @RequestBody Clientes cliente) {
        return clientesRepository.save(cliente);
    }

    @PutMapping("/clientes/{clienteId}")
    public Clientes updateCliente(@PathVariable Long clienteId,
                                   @Valid @RequestBody Clientes clientesRequest) {
        return clientesRepository.findById(clienteId)
                .map(clientes -> {
                    clientes.setNome_cliente(clientesRequest.getNome_cliente());
                    clientes.setTipo_cliente(clientesRequest.getTipo_cliente());
                    clientes.setSituacao_cliente(clientesRequest.getSituacao_cliente());                    
                    
                    return clientesRepository.save(clientes);
                }).orElseThrow(() -> new ResourceNotFoundException("Cliente nao encontrado com id " + clienteId));
    }


    @DeleteMapping("/clientes/{clienteId}")
    public ResponseEntity<?> deleteCliente(@PathVariable Long clienteId) {
        return clientesRepository.findById(clienteId)
                .map(clientes -> {
                    clientesRepository.delete(clientes);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Cliente nao encontrado com id " + clienteId));
    }
}
