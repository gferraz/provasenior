package com.example.postgresdemo.controller;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model.Produtos;
import com.example.postgresdemo.repository.ProdutosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
public class ProdutosController {

    @Autowired
    private ProdutosRepository produtosRepository;

    @GetMapping("/produtos")
    public Page<Produtos> getProduto(Pageable pageable) {
        return produtosRepository.findAll(pageable);
    }


    @PostMapping("/produtos")
    public Produtos createProduto(@Valid @RequestBody Produtos produto) {
        return produtosRepository.save(produto);
    }

    @PutMapping("/produtos/{produtoId}")
    public Produtos updateProduto(@PathVariable Long produtoId,
                                   @Valid @RequestBody Produtos produtosRequest) {
        return produtosRepository.findById(produtoId)
                .map(produtos -> {   
                	produtos.setCodigo_produto(produtosRequest.getCodigo_produto());
                	produtos.setDescricao_produto(produtosRequest.getDescricao_produto());
                	if(produtosRequest.getSituacao_produto().isEmpty()) {
                		produtos.setSituacao_produto("A");
                	}else
                		produtos.setSituacao_produto(produtosRequest.getSituacao_produto());
                	
                	if(produtosRequest.getTipo_produto().isEmpty()) {
                		produtos.setTipo_produto("P");
                	}else
                		produtos.setTipo_produto(produtosRequest.getTipo_produto());
                	
                	produtos.setValor_produto(produtosRequest.getValor_produto());
                    
                    return produtosRepository.save(produtos);
                }).orElseThrow(() -> new ResourceNotFoundException("Produto nao encontrado com id " + produtoId));
    }


    @DeleteMapping("/produtos/{produtoId}")
    public ResponseEntity<?> deleteProduto(@PathVariable Long produtoId) {
        return produtosRepository.findById(produtoId)
                .map(produtos -> {
                    produtosRepository.delete(produtos);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Produto nao encontrado com id " + produtoId));
    }
}
