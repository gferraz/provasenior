package com.example.postgresdemo.controller;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model.MestrePedido;
import com.example.postgresdemo.repository.MestrePedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
public class PedidosController {

    @Autowired
    private MestrePedidoRepository mestrePedidoRepository;

    @GetMapping("/pedidos")
    public Page<MestrePedido> getPedido(Pageable pageable) {
        return mestrePedidoRepository.findAll(pageable);
    }


    @PostMapping("/pedidos")
    public MestrePedido createPedido(@Valid @RequestBody MestrePedido pedido) {
        return mestrePedidoRepository.save(pedido);
    }

    @PutMapping("/pedidos/{pedidoId}")
    public MestrePedido updatePedido(@PathVariable Long pedidoId,
                                   @Valid @RequestBody MestrePedido pedidosRequest) {
        return mestrePedidoRepository.findById(pedidoId)
                .map(pedidos -> {
                	pedidos.setSituacao_pedido(pedidosRequest.getSituacao_pedido());
                	pedidos.setValor_acrescimo(pedidosRequest.getValor_acrescimo());
                	pedidos.setValor_desconto(pedidosRequest.getValor_desconto());
                	pedidos.setValor_produtos(pedidosRequest.getValor_produtos());
                	pedidos.setValor_total(pedidosRequest.getValor_total());
                    return mestrePedidoRepository.save(pedidos);
                }).orElseThrow(() -> new ResourceNotFoundException("Pedido nao encontrado com id " + pedidoId));
    }


    @DeleteMapping("/pedidos/{pedidoId}")
    public ResponseEntity<?> deletePedido(@PathVariable Long pedidoId) {
        return mestrePedidoRepository.findById(pedidoId)
                .map(pedidos -> {
                	mestrePedidoRepository.delete(pedidos);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Pedido nao encontrado com id " + pedidoId));
    }
}
